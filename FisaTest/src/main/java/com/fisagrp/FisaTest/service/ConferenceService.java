package com.fisagrp.FisaTest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fisagrp.FisaTest.entity.Conference;
import com.fisagrp.FisaTest.entity.Proposal;
import com.fisagrp.FisaTest.entity.Track;

/**
 * @author USUARIO
 *
 */
/**
 * @author USUARIO
 *
 */
@Service
public class ConferenceService {

	Conference conference;

	/**
	 * 
	 */
	public ConferenceService() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the conference
	 */
	public Conference getConference() {
		return conference;
	}

	/**
	 * @param conference the conference to set
	 */
	public void setConference(Conference conference) {
		this.conference = conference;
	}

	/**
	 * Create a static List of Proposal to include in the conference
	 */
	public void initalizeConference() {
		this.conference = new Conference();
		this.conference.createStaticProposals();
	}

	/**
	 * Create a track which includes a Morning session and an Early or Late Afternoon 
	 * @return Track
	 */
	public Track createTrack() {
		Track track = new Track();
		track.createTrack();
		return track;
	}

	
	/**
	 * Convert a list of Proposals to talks for a Track
	 * 
	 * @param proposals list of proposals
	 * @param track		track where the talks are set
	 */
	public void addTalksToTrack(List<Proposal> proposals, Track track) {
		track.addTalksToTrack(proposals);
	}

	public void generateConferenceTrack(int trackListSize) {

		List<Proposal> proposals = new ArrayList<Proposal>();
		proposals.addAll(this.conference.getProposals());

		int trackSelected = 1;

		while (trackSelected <= trackListSize) {
			List<Proposal> aleatorProposals = new ArrayList<Proposal>();
			aleatorProposals.addAll(this.conference.createAleatorySortProposalList(proposals));
			List<Proposal> insertProposals = new ArrayList<Proposal>();
			insertProposals.addAll(aleatorProposals);

			Track track = new Track();
			track = this.createTrack();

			if (track.evaluateTrack(aleatorProposals)) {
				this.addTalksToTrack(insertProposals, track);
				this.conference.getTracks().add(track);
				trackSelected++;
			}
		}

		System.out.println("");
		System.out.println("Test Input:   ");
		showProposals();
		System.out.println("");
		System.out.println("Output:   ");
		showTracks();

	}

	/**
	 * Method used to print the list of proposals in console 
	 */
	public void showProposals() {
		this.conference.showProposals();
	}

	/**
	 * Method used to print the list of tracks in console 
	 */
	public void showTracks() {
		this.conference.showTracks();
	}
}
