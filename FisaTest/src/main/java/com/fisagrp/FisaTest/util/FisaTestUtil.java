package com.fisagrp.FisaTest.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.fisagrp.FisaTest.exception.FisaTestException;

/**
 * @author USUARIO
 *
 */
/**
 * @author USUARIO
 *
 */
public class FisaTestUtil {

	/**
	 * @param hour
	 * @return
	 */
	public static Date getHourDate( String hour){
		SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
		Date hourAsDate;
		try {
			hourAsDate = hourFormat.parse(hour);
		} catch (ParseException e) {
			throw new FisaTestException("The hour registered is not correct. Please insert an hour in format HH:mm.");
		}
		return hourAsDate;
	}
	
	
	/**
	 * @param startHour
	 * @param endHour
	 * @return
	 */
	public static long getTimeDiference(Date startHour, Date endHour) {
		   if (startHour.after(endHour)) {
			   throw new FisaTestException("The end hour is earlier the start hour. Please insert another hour.");
		   }
		    long dateDiference=endHour.getTime() - startHour.getTime();
			long minutesDiference = TimeUnit.MILLISECONDS.toMinutes(dateDiference); 
			return minutesDiference;
	}
	
	
	/**
	 * This code section was taken from the reply of @gonzo reply in https://stackoverflow.com/questions/33636887/generating-10-random-numbers-without-duplicate-with-fundamental-techniques
	 * this is a generic code to generate random numbers
	 * 
	 * @param listSize
	 * @return
	 */
	public static ArrayList<Integer> createAleatoryNumberList(int listSize, int numberAleatoryValues) {
		
		ArrayList<Integer> aleatorylist = new ArrayList<Integer>();
		
		Random r = new Random();
		Set<Integer> uniqueNumbers = new HashSet<Integer>();
		while (uniqueNumbers.size()<=listSize){
		    //uniqueNumbers.add(r.nextInt(41));
			uniqueNumbers.add(r.nextInt(numberAleatoryValues));
		}
		for (Integer i : uniqueNumbers){
			aleatorylist.add(i);
		}
		return aleatorylist;
	}
	
	public static Date addMinutesToDate(Date sourceDate, int minutes) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sourceDate);
		calendar.add(Calendar.MINUTE, minutes);
		
		return calendar.getTime();
		
	}
	
	
public static String fromDateToHour(Date dateSource) {
	
	Calendar calendar = Calendar.getInstance();
    calendar.setTime(dateSource);
    
	LocalDateTime localDateTime = LocalDateTime.of(2000, 1, 1, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0, 0);

	 DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a");
     String formatHour = dateTimeFormatter.format(localDateTime);
     
     return String.format("%s" , formatHour);
	}
	
	}
