/**
 * 
 */
package com.fisagrp.FisaTest.util;

import java.util.Comparator;

import com.fisagrp.FisaTest.entity.Proposal;

/**
 * @author USUARIO
 *
 */
public class ProposalComparator implements Comparator<Proposal> {

	
	    public int compare(Proposal a, Proposal b) {
	        return a.getOrder() - b.getOrder();
	    }
	
}
