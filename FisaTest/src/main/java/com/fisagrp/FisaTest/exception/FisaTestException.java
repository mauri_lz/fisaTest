package com.fisagrp.FisaTest.exception;

public class FisaTestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3058326794540261124L;

	public FisaTestException(String msg) {
		super(msg);
	    }
}
