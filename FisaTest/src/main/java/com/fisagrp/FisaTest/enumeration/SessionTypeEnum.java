package com.fisagrp.FisaTest.enumeration;

/**
 * @author Mauricio Toapanta
 *
 */
public enum SessionTypeEnum {
	
	MORNING(1,"9:00","12:00"),EARLY_AFTERNOON(2,"13:00","16:00"), LATE_AFTERNOON(2,"13:00","17:00");
	
	/**
	 * 
	 */
	private Integer sessionType;
	private String startTime;
	private String EndTime;
	
	/**
	 * @param sessionType
	 * @param startTime
	 * @param endTime
	 */
	private SessionTypeEnum(Integer sessionType, String startTime, String endTime) {
		this.sessionType = sessionType;
		this.startTime = startTime;
		EndTime = endTime;
	}

	/**
	 * @return the sessionType
	 */
	public Integer getSessionType() {
		return sessionType;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return EndTime;
	}


	


}
