package com.fisagrp.FisaTest.enumeration;

public enum ProposalEnum {
	
	PRO1("Writing Fast Tests Against Enterprise Rails",60),
	PRO2("Overdoing it in Python",45),
	PRO3("Lua for the Masses",30),
	PRO4("Ruby Errors from Mismatched Gem Versions",45),
	PRO5("Common Ruby Errors",45),
	PRO6("Rails for Python Developers",5),
	PRO7("Communicating Over Distance",60),
	PRO8("Accounting-Driven Development",45),
	PRO9("Woah",30),
	PRO10("Sit Down and Write",30),
	PRO11("Pair Programming vs Noise",45),
	PRO12("Rails Magic",60),
	PRO13("Ruby on Rails: Why We Should Move On",60),
	PRO14("Clojure Ate Scala (on my project)",45),
	PRO15("Programming in the Boondocks of Seattle",30),
	PRO16("Ruby vs. Clojure for Back-End Development",30),
	PRO17("Ruby on Rails Legacy App Maintenance",60),
	PRO18("A World Without HackerNews",30),
	PRO19("User Interface CSS in Rails Apps",30);

	private String topic;
	private long timeInterval;
	
	/**
	 * @param topic
	 * @param timeInterval
	 */
	private ProposalEnum(String topic, Integer timeInterval) {
		this.topic = topic;
		this.timeInterval = timeInterval;
	}

	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * @return the timeInterval
	 */
	public long getTimeInterval() {
		return timeInterval;
	}

	
	
	
}
