package com.fisagrp.FisaTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.fisagrp.FisaTest.service.ConferenceService;

@SpringBootApplication
public class FisaTestApplication  {

	@Autowired
	private ConferenceService conferenceService;

	public static void main(String[] args) {
		SpringApplication.run(FisaTestApplication.class, args);
	}


	@EventListener({ApplicationReadyEvent.class})
    public void initOnStartAplication() {
	
		System.out.println("Running Application...");
		conferenceService.initalizeConference();
		conferenceService.generateConferenceTrack(2);
	}
}
