package com.fisagrp.FisaTest.entity;

public class Proposal {

	private String topic;
	private long timeInterval;
	private int order;

	public Proposal(String topic, long timeInterval) {
		super();
		this.topic = topic;
		this.timeInterval = timeInterval;
	}

	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	/**
	 * @return the timeInterval
	 */
	public long getTimeInterval() {
		return timeInterval;
	}

	/**
	 * @param timeInterval the timeInterval to set
	 */
	public void setTimeInterval(long timeInterval) {
		this.timeInterval = timeInterval;
	}

	/**
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	public void showProposal() {
		System.out.println( String.format("%s  -  %s min" , this.topic, this.timeInterval));  
	}
	

	
}
