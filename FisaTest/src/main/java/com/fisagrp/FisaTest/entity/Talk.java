package com.fisagrp.FisaTest.entity;

import java.util.Date;

import com.fisagrp.FisaTest.util.FisaTestUtil;

public class Talk {
	private String topic;
	private Date startHour;
	private Date endHour;
	
	
	/**
	 * 
	 */
	public Talk() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param topic
	 * @param startHour
	 * @param endHour
	 */
	public Talk(String topic, Date startHour, Date endHour) {
		super();
		this.topic = topic;
		this.startHour = startHour;
		this.endHour = endHour;
	}

	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}

	/**
	 * @return the startHour
	 */
	public Date getStartHour() {
		return startHour;
	}

	/**
	 * @param startHour the startHour to set
	 */
	public void setStartHour(Date startHour) {
		this.startHour = startHour;
	}

	/**
	 * @return the endHour
	 */
	public Date getEndHour() {
		return endHour;
	}

	/**
	 * @param endHour the endHour to set
	 */
	public void setEndHour(Date endHour) {
		this.endHour = endHour;
	}

	public void createTalkFromProposal(Date startHour, Proposal proposal) {
				this.setStartHour( startHour);
				this.setEndHour(FisaTestUtil.addMinutesToDate(startHour,Long.valueOf(proposal.getTimeInterval()).intValue()));
				this.setTopic(proposal.getTopic()); 
	}
	
	public void printTalk() {
		System.out.println( String.format("  %s: %s " , FisaTestUtil.fromDateToHour(this.startHour), this.topic));
	}
}
