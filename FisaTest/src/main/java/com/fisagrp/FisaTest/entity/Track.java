package com.fisagrp.FisaTest.entity;

import java.util.List;

import com.fisagrp.FisaTest.enumeration.SessionTypeEnum;
import com.fisagrp.FisaTest.util.FisaTestUtil;

public class Track {
	
	private Session morningSession;
	private Session afternoonSession;
	
	
	
	/**
	 * 
	 */
	public Track() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param morningSession
	 * @param afternoonSession
	 */
	public Track(Session morningSession, Session afternoonSession) {
		super();
		this.morningSession = morningSession;
		this.afternoonSession = afternoonSession;
	}


	/**
	 * @return the morningSession
	 */
	public Session getMorningSession() {
		return morningSession;
	}


	/**
	 * @param morningSession the morningSession to set
	 */
	public void setMorningSession(Session morningSession) {
		this.morningSession = morningSession;
	}


	/**
	 * @return the afternoonSession
	 */
	public Session getAfternoonSession() {
		return afternoonSession;
	}


	/**
	 * @param afternoonSession the afternoonSession to set
	 */
	public void setAfternoonSession(Session afternoonSession) {
		this.afternoonSession = afternoonSession;
	}


	/**
	 * Create a track which includes a Morning session and an Early or Late Afternoon 
	 * 
	 */
	public void createTrack () {
		
		this.morningSession = new Session(SessionTypeEnum.MORNING);
		
		if(FisaTestUtil.createAleatoryNumberList(1,2).get(0)==0) {
			this.afternoonSession = new Session(SessionTypeEnum.EARLY_AFTERNOON);
		}else {
			this.afternoonSession = new Session(SessionTypeEnum.LATE_AFTERNOON);
		}
	}
	
	
	public boolean evaluateTrack(List<Proposal> proposals) {
		
		if(this.morningSession.evaluateSession( proposals)) {
			if(this.afternoonSession.evaluateSession( proposals)) {
				return true;
			}
			
		}
		return false;
		
	}
	
	public void addTalksToTrack(List<Proposal> proposals) {
		this.getMorningSession().addTalksToSession(proposals);
		this.getAfternoonSession().addTalksToSession(proposals);
	}
	
	/**
	 * Method used to print the Morning and Afternoon session of a track in console 
	 */
	public void showSessions() {
		System.out.println("Morning Session");
		this.getMorningSession().showSessionTalks();
		System.out.println("Afternoon Session");
		this.getAfternoonSession().showSessionTalks();
	}
	
}
