package com.fisagrp.FisaTest.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fisagrp.FisaTest.enumeration.SessionTypeEnum;
import com.fisagrp.FisaTest.util.FisaTestUtil;

public class Session {

	private List<Talk> talks;
	private Date startHour;
	private Date endHour;
	private long minutesInterval;
	private SessionTypeEnum sessionTypeEnum;
	
	/**
	 * @param sessionTypeEnum
	 */
	public Session(SessionTypeEnum sessionTypeEnum) {
		super();
		this.sessionTypeEnum = sessionTypeEnum;
		this.generateSession(sessionTypeEnum);
	}
	

	/**
	 * @return the talks
	 */
	public List<Talk> getTalks() {
		return talks;
	}

	/**
	 * @param talks the talks to set
	 */
	public void setTalks(List<Talk> talks) {
		this.talks = talks;
	}

	/**
	 * @return the startHour
	 */
	public Date getStartHour() {
		return startHour;
	}

	/**
	 * @return the endHour
	 */
	public Date getEndHour() {
		return endHour;
	}

	/**
	 * @return the minutesInterval
	 */
	public long getMinutesInterval() {
		return minutesInterval;
	}

	/**
	 * @return the sessionTypeEnum
	 */
	public SessionTypeEnum getSessionTypeEnum() {
		return sessionTypeEnum;
	}

	private void generateSession(SessionTypeEnum sessionTypeEnum) {
		this.talks = new ArrayList<Talk>();
		this.startHour = FisaTestUtil.getHourDate(sessionTypeEnum.getStartTime());
		this.endHour = FisaTestUtil.getHourDate(sessionTypeEnum.getEndTime());
		this.minutesInterval =  FisaTestUtil.getTimeDiference(startHour, endHour);
	}
		
	public boolean evaluateSession(List<Proposal> proposals) {
		
		//Evaluate Session
		long minutes = 0;
		List<Proposal> proposalDelete =new ArrayList<Proposal> ();
		
		for(Proposal proposal : proposals) {
			minutes = minutes +proposal.getTimeInterval();
			proposalDelete.add(proposal);
		
			if (this.getMinutesInterval() == minutes) {
				break;
			}
			if ( minutes > this.getMinutesInterval() ) {
				return false;
			}
			
		}
		
		proposals.removeAll(proposalDelete);
		
		return true;
		
	}
	
	public void addTalksToSession(List<Proposal> proposals) {
		
		List<Talk> talks = new ArrayList<Talk>();
		Date startSessionHour = this.startHour;
		
		List<Proposal> proposalEliminar = new ArrayList<Proposal>();
		
		for(Proposal proposal : proposals) {
			
			Talk talk = new Talk();
			talk.createTalkFromProposal(startSessionHour, proposal);
			talks.add(talk);
			startSessionHour = talk.getEndHour();
			proposalEliminar.add(proposal);
			
			if(startSessionHour.compareTo(this.endHour)==0) {
				break;
			}
		}
		
		if (!this.sessionTypeEnum.equals(SessionTypeEnum.MORNING)) {
			Date networkingEventDate = FisaTestUtil.getHourDate(this.sessionTypeEnum.getEndTime());
			Talk networkingEventTalk=new Talk("Networking Event", networkingEventDate, networkingEventDate );
			talks.add(networkingEventTalk);
		}
		proposals.removeAll(proposalEliminar);
		this.setTalks(talks);
		
	}
	
	/**
	 * Method used to print the talks of a session  
	 */
	public void showSessionTalks() {
		for(Talk talk : this.getTalks()) {
			talk.printTalk();
		}
	}
}
