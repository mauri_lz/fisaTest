package com.fisagrp.FisaTest.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fisagrp.FisaTest.enumeration.ProposalEnum;
import com.fisagrp.FisaTest.util.FisaTestUtil;
import com.fisagrp.FisaTest.util.ProposalComparator;

public class Conference {
	
	private List<Proposal> proposals;
	private List<Track> tracks;
	/**
	 * @param proposals
	 */
	public Conference() {
		super();
		this.proposals = new ArrayList<Proposal>();
		this.tracks = new ArrayList<Track>();
	}
	
	
	
	/**
	 * @param proposals
	 * @param tracks
	 */
	public Conference(List<Proposal> proposals, List<Track> tracks) {
		super();
		this.proposals = proposals;
		this.tracks = tracks;
	}



	/**
	 * @return the proposals
	 */
	public List<Proposal> getProposals() {
		return proposals;
	}



	/**
	 * @param proposals the proposals to set
	 */
	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}



	/**
	 * @return the tracks
	 */
	public List<Track> getTracks() {
		return tracks;
	}



	/**
	 * @param tracks the tracks to set
	 */
	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}



	public void createStaticProposals() {
		this.proposals  = new ArrayList<Proposal>();
		
		for(ProposalEnum proposalEnum: ProposalEnum.values() ) {
			Proposal proposal = new Proposal(proposalEnum.getTopic(), proposalEnum.getTimeInterval());
			this.proposals.add(proposal);
		}
		
	}
	
	public  List<Proposal> createAleatorySortProposalList(List<Proposal> proposals) {
		
		ArrayList<Integer> aleatoryList = FisaTestUtil.createAleatoryNumberList(proposals.size(), 41);
		int listIterator = 1;
		
		for(Proposal proposal : proposals) {
			proposal.setOrder(aleatoryList.get(listIterator));	
			listIterator++;
		}
		
		Collections.sort(proposals, new ProposalComparator());
		
		return proposals;
		
	}
	
	
	public void showProposals() {
		for(Proposal proposal : this.getProposals()) {
			proposal.showProposal();
		}
	}
	
	/**
	 * Method used to print the Morning and Afternoon session of a track in console 
	 */
	public void showTracks() {
		
		int tracksSize = 1;
		for(Track track : this.getTracks()) {
			System.out.println("");
			System.out.println(String.format("Track Number %s " , tracksSize ));
			track.showSessions();
			tracksSize++;
		}
	}
}
