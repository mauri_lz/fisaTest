package com.fisagrp.FisaTest;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fisagrp.FisaTest.service.ConferenceService;

@SpringBootTest
public class ConferenceServiceTests {

	@Autowired
	ConferenceService confereceService = new ConferenceService();
	
	/*
	 * Test if the list of static proposals were generated
	 */
	@Test
	public void initalizeConferenceNoEmptyTest() {
		//Initialize the proposals list
		this.confereceService.initalizeConference();
		
		assertNotEquals(this.confereceService.getConference().getProposals().size(), 0);
	}
	
	/*
	 * Test if tracks were generated
	 */
	@Test
	public void generateConferenceTrackNoEmptyTest() {
		//Initialize the proposals list
		this.confereceService.initalizeConference();
		//Generate two tracks
		this.confereceService.generateConferenceTrack(2);
		
		assertNotEquals(this.confereceService.getConference().getTracks().size(), 0);
	}
	
	/*
	 * Compare the number of  tracks generated and the number of tracks required
	 */
	@Test
	public void generateConferenceTrackCorrectGenerationTest() {
		int trackGeneratedNumber =  2;
		//Generates two tracks
		this.confereceService.initalizeConference();
		this.confereceService.generateConferenceTrack(trackGeneratedNumber);
		
		assertEquals(this.confereceService.getConference().getTracks().size(), trackGeneratedNumber);
	}
	
	/*
	 * Compare the startHour between the seesions and their first talk in a track
	 */
	@Test
	public void startHourConferenceTest() {
		//Generates a track
		int trackGeneratedNumber =  1;
		this.confereceService.initalizeConference();
		this.confereceService.generateConferenceTrack(trackGeneratedNumber);
		
		//Get the morning session start hour
		Date startHourMorningSession = this.confereceService.getConference().getTracks().get(0).getMorningSession().getStartHour();
		//Get the first talk morning session start hour
		Date startHourMorningFirsTalk =this.confereceService.getConference().getTracks().get(0).getMorningSession().getTalks().get(0).getStartHour();
		
		assertEquals(startHourMorningSession,startHourMorningFirsTalk);

		//Get the afternoon session start hour
		Date startHourAfternoonSession = this.confereceService.getConference().getTracks().get(0).getAfternoonSession().getStartHour();
		//Get the first talk afternoon session start hour
		Date startHourAfternoonFirsTalk =this.confereceService.getConference().getTracks().get(0).getAfternoonSession().getTalks().get(0).getStartHour();
		
		assertEquals(startHourAfternoonSession,startHourAfternoonFirsTalk);
	}

	/*
	 * Compare the endHour between the seesions and their last talk in a track (Network Event start hour in case of afternoon session)
	 */
	@Test
	public void endHourConferenceTest() {
		//Generates a track
		int trackGeneratedNumber =  1;
		this.confereceService.initalizeConference();
		this.confereceService.generateConferenceTrack(trackGeneratedNumber);
		
		//Get the morning session end hour
		Date endHourMorningSession = this.confereceService.getConference().getTracks().get(0).getMorningSession().getEndHour();
		//Get the last talk morning session end hour
		int morningTalkListSize =this.confereceService.getConference().getTracks().get(0).getMorningSession().getTalks().size();
		Date endHourMorningLastTalk =this.confereceService.getConference().getTracks().get(0).getMorningSession().getTalks().get(morningTalkListSize-1).getEndHour();
		
		assertEquals(endHourMorningSession,endHourMorningLastTalk);

		//Get the afternoon session end hour
		Date endHourAfternoonSession = this.confereceService.getConference().getTracks().get(0).getAfternoonSession().getEndHour();
		//Get the network event start hour
		int afternoonTalkListSize =this.confereceService.getConference().getTracks().get(0).getAfternoonSession().getTalks().size();
		Date endHourAfternoonLastTalk =this.confereceService.getConference().getTracks().get(0).getAfternoonSession().getTalks().get(afternoonTalkListSize-1).getStartHour();
		
		assertEquals(endHourAfternoonSession,endHourAfternoonLastTalk);
	}
}
